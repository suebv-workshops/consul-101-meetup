# Consul 101 Meetup

Deze repository is bedoeld om mensen kennis te maken met Consul applicatie van
Hashicorp.

## Eisen voor de Meetup

De volgende punten zijn nodig voor deze Meetup:

- Werkstation met SSH mogelijkheid
- Internet toegang
- Kennis van een command-line editor. (v.b. VI(m), emacs, nano, etc.)
- Enige kennis van *NIX. (Linux, FreeBSD, etc.)

## Opdrachten

- 101: Consul
- 102: Consul k/v datastore
- 103: Consul Service Discovery
- 104: Consul Connect
- 105: Consul ACLs
