# Consul 101 Meetup: Deploy Consul server

Deze opdracht gaan we een Consul server en agent opstarten en paar basis 
commando's uitvoeren.

## Lab

### 101.1

Consul is een *single binary* applicatie. Om Consul op te starten voor deze
meetup gebruiken we de `dev` modus van Consul.

```bash
consul agent -dev -enable-script-checks -ui
```

Open een nieuw terminal scherm en controleer of consul actief is.

```bash
consul members
consul catalog services
```
