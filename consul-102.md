# Consul 101 Meetup: K/V Store

Bij deze opdracht gaan we gebruik maken van de key-value store die meekomt met Consul.

## Labs

### 102.1

- Maak een key aan met de naam `sue/meetup/name` en zet daar de waarde `consul` erin.
- Probeer vervolgens de waarde weer uit te lezen.

### 102.2

Probeer de waardes nu via [API parameters](https://www.consul.io/api-docs/kv) op te vragen.

v.b. listing van opgegeven key:

```
curl '127.0.0.1:8500/v1/kv/?keys'
```

