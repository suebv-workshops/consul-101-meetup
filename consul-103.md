# Consul 101 Meetup: Service Discovery

In deze opdracht gaan we servers registreren in Consul.
Op de server draait een Nginx webserver. Deze kunnen we registreren als een 
service van de server, zodat we die daarna kunnen opvragen als dienst.

## 103.1

Om een service te registreren zijn er verschillende manieren om deze te 
registeren:
- config folder
- consul commando
- API

Maak een bestand aan met de extentie `.hcl` en voeg onderstaande toe:

```hcl
service {
  name = "web"
  id = "web-1"
  port = 80
}
```
Via het consul commando kunnen we deze registreren.

```bash
consul services register web.hcl
```

Vervolgens kunnen we via `consul catalog services` zien dat de service is
geregistreerd. Dit is ook te zien via de API of via de UI.

```bash
curl 127.0.0.1:8500/v1/agent/services
curl 127.0.0.1:8500/v1/agent/service/web
```
## 103.2

We hebben nu een service toegevoegd, maar als de nginx op de server stoppen
wat gebeurt er met de service?

## 103.3

Voeg het volgende toe aan de configuratie:

```hcl
  check {
    http = "http://localhost"
    method = "GET"
    interval = "30s"
  }
```

Voeg de wijzigingen toe aan Consul en controleer wat er nu te zien is als de 
nginx service weer gestart wordt? (of juist weer stopt)
