# Consul 101 Meetup: Connect services

Sinds de release van Consul versie 1.2 is nu ook de mogelijkheid om services die
geregistreerd zijn op de Consul agents met elkaar te laten communiceren in een
veilige manier.
Hiermee kan je dan een volledige *service mesh* opzetten voor je diensten.

Consul Connect maakt gebruikt van *mutual TLS* (of terwijl mTLS) en een sidecar
proxy om de communicatie tussen services op te zetten en veilig te laten 
communiceren.

## 104.1

We gaan een web applicatie starten en registreren die gebruik gaat maken van
service connect, om zo beter inzicht te krijgen hoe Consul Service Connect werkt.

```
               +--------------+               +-------------+
               | |-------|    |               |    |--------|
+----+         | +-------+    |               |    +--------|
|    | +--------->       |   +--+ <-------> +--+   |       ||
|    |         | |       |   +--+           +--+   |       ||
+----+         | |       |    |               |    |       ||
CLIENT         | +-------+    |               |    +--------|
               +--------------+               +-------------+
                   DASHBOARD                      COUNTING
```

In de home directory tref je twee applicaties aan:

- [counting-service](https://github.com/hashicorp/katakoda/raw/master/consul-connect/assets/bin/counting-service)
- [dashboard-service](https://github.com/hashicorp/katakoda/raw/master/consul-connect/assets/bin/dashboard-service)

We gaan voor deze test uit dat de *counting-service* onze database is en de
*dashboard-service* onze web applicatie die gebruik wilt maken van
de *counting-service*.

Om dit te realiseren is het handig om meerdere terminal schermen te openen of
gebruik te maken van een *multiplexer*.

Start de *counting-service* op met volgende commando en deze zal dan lokaal een
service starten op de aangegeven poort, `9001`.

```bash
PORT=9001 counting-service > counting.log &
```

Registreer nu deze service via de API van Consul:

```hcl
service {
  name = "counting"
  id   = "counting-1"
  port = 9001

  check {
    id       = "counting-web"
    http     = "http://localhost:9001/health"
    method   = "GET"
    interval = "1s"
    timeout  = "1s"
  }

  connect {
    sidecar_service {}
  }
}
```

Laat de service configuratie in en controleer of de service healthy is. Je zult
zien dat de service nog niet *healthy* is, maar de health check wel goed is. 

## 104.2

Nu dat we de *database* service hebben opgestart kunnen we de frontend applicatie
gaan toevoegen.

Eerst registreren we de service in Consul met onderstaande configuratie:

```hcl
service {
  name = "dashboard"
  port = 9002

  check {
    id       = "dashboard-web"
    http     = "http://localhost:9002/health"
    method   = "GET"
    interval = "1s"
    timeout  = "1s"
  }

  connect {
    sidecar_service {
      proxy {
        upstreams = [
          {
            destination_name = "counting"
            local_bind_port  = 9101
          }
        ]
      }
    }
  }
}
```

En starten we de dashboard service op:

```bash
PORT=9002 COUNTING_SERVICE_URL=http://localhost:9101 \
  dashboard-service > dashboard.log &
```

Eenmaal de service te hebben ingeladen zien we ook dat deze niet healthy is.
Dit komt omdat de sidecar proxy service nog niet geactiveerd is. Dit gaan we nu 
met de hand doen. Voor Kubernetes en Nomad is het mogelijk dat de betreffende 
orchestratie platform dit voor jou doet.

## 104.3
Om gebruik te maken van de sidecar proxy, is het nodig om een proxy service op
te starten. Consul heeft een eigen ingebouwde proxy functionaliteit, maar je kan
ook (v.b.) Envoy gebruiken. Voor productie adviseren ze dan ook om Envoy te
gebruiken i.p.v. de simpele versie.

```bash
consul connect proxy -sidecar-for counting-1 > couting-sidecar.log &
consul connect proxy -sidecar-for dashboard > dashboard-sidecar.log &
```

Nadat beide proxies zijn opgestart zouden beide nu status healthy moeten hebben.

## 104.4

Om te voorkomen dat alle diensten met elkaar kunnen communiceren via een service
mesh zit er een *access control* functionaliteit in. Dit wordt geregeld 
via *intentions*.

Intentions kunnen we instellen via de `consul intention`, UI of via API.

Default wordt alles toegelaten, nu wil je dit niet in productie, dus gaan we
eerst *default deny* uitvoeren:

```bash
consul intention create -deny \* \*
```

Als je nu de dashboard benaderd dan zie je dat de dienst niet meer beschikbaar
is. Dit komt omdat de policy zorgt dat de er nu geen services met elkaar mag
communiceren.

Nu willen we de dashboard wel met de counting laten communiceren:

```bash
consul intention create -allow dashboard counting
```

Als je nogmaals de dashboard bekijk dan zal je zien dat de dienst nu weer werkt.
