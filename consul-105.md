# Consul 101 Meetup: Consul ACL

Standaard kan iedereen bij de Consul server komen. Voor productie is dit niet 
wenselijk. Je zou dit met firewalls kunnen oplossen, maar Consul heeft ook de
mogelijkheid om dit via *access control list* op te lossen.

## 105.1

Om ACL te kunnen testen moeten we Consul gaan opstarten met ACL functionaliteit.
Dit kan als argument, maar ook via een config file.

Maak een sub-folder aan en zet daar een file neer (v.b. `consul.hcl`) met de 
volgende inhoud:

```hcl
bind_addr = "127.0.0.1"
server = true
ui = true

acl {
  enabled = true
  default_policy = "deny"
}
```

Maak ook een data folder aan om de database in weg te schrijven.
Start vervolgens Consul op met de volgende argumenten:

```bash
consul agent -config-file=consul.hcl -data-dir=data -bootstrap > consul.log &
```

Wat gebeurd er als je nu de node lijst bekijk? Of als je de DNS van Consul 
benaderd?

## 105.2

Om ACL te laten werken moet er eerst een master token aangemaakt worden. Deze
token zorgt ervoor dat ter aller tijden Consul wijzigingen uitgevoerd kunnen
worden.

```bash
consul acl bootstrap
```

Je krijgt bericht terug dat een token is aangemaakt. In de output zie je een
*accessor id* en een *secret id*. De *accessor id* is een soort van unieke ID 
verwijzing naar de token. De *secret id* is de daadwerkelijke token die
gebruikt kan worden.

Als we de *secret id* nu in een environment variabel zetten, `CONSUL_HTTP_TOKEN`
en dan de lijst met nodes opvragen, wat gebeurd er dan?

## 105.3

Nu dat de master token is aangemaakt kunnen we voor de Consul agent een token
aanmaken, zodat de agent netjes met Consul kan communiceren.
Hiervoor hebben we een policy nodig.

```hcl
node_prefix "" {
  policy = "write"
}

service_prefix "" {
  policy = "read"
}
```

```bash
consul acl policy create -name agent -rules @agent.hcl
```

Nu hebben we een policy aangemaakt en kunnen op basis van een policy een token
aanmaken.

```bash
consul acl token create -description "Agent token" -policy-name agent
```

Nu zal je zien dat Consul agent zijn info kan bijwerken in Consul.
Is het nu wel mogelijk om alle nodes te zien?

## 105.4

Maak de volgende policy aan en koppel deze aan de *anonymous* token.

```hcl
node_prefix "" {
  policy = "read"
}

agent_prefix "" {
  policy = "read"
}

service_prefix "" {
  policy = "read"
}
```
