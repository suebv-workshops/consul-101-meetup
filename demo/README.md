# Consul Meetup 101: Demo applicatie

Om Consul Service Connect goed te laten zien maken we gebruik van twee test
applicaties.

- Counter
- Dashboard

De bedoeling is om de de dashboard met de counting service te laten communiceren
via Consul.

## Eisen

Om de applicaties te kunnen gebruiken moeten deze eerst gecompileerd worden.
Beide applicaties zijn op basis van Go geschreven.

## Voorbereiding

Om de applicaties te kunnen gebruiken moet de instructor de applicaties
compileren.

Vanuit elke folder de `bin/build` commando uitvoeren.
